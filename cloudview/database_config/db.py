# This file holds the database setting for development and production environment

import os

environment = os.environ.get('cloudview_environment')

# Common database settings
port = '5432'

# Database setting for development environment
if (environment == 'Development'):	
	host = ''
	name = 'cloudview_db'
	user = 'cloudview_admin'
	password = 'password'

	
# Database setting for production environment
if (environment == 'Production'):
	host = 'cloudviewdbv1.cwb6zrveg9av.us-east-1.rds.amazonaws.com'
	name = 'cloudviewdb'
	user = 'cloudviewadmin'
	password = 'cloudviewpassword'

else:
	host = 'cloudviewdbv1.cwb6zrveg9av.us-east-1.rds.amazonaws.com'
	name = 'cloudviewdb'
	user = 'cloudviewadmin'
	password = 'cloudviewpassword'