import pytz
from datetime import datetime, date, timedelta

from django.shortcuts import render, redirect
from django.contrib.auth.decorators import login_required

import pandas as pd

from recognition.models import AnalysisData, Notification, NotificationHistory, Person, PersonFace
from catalog.models import Catalog

# Create your views here.
@login_required(redirect_field_name='login')
def report(request):
	user = request.user
	print (user)
	catalogs = Catalog.objects.filter(user_id=user)
	# Get all the parameters of get request
	start_date = request.GET.get('start_date')
	end_date = request.GET.get('end_date')
	catalog = request.GET.get('catalog')
	if (catalog==None):
		catalog = 'ALL'
	try:
		timezoneoffset = int(request.GET.get('timezone'))
	except:
		timezoneoffset = 0
	# Convert the datetime string to datetime object (local time)
	start_date_local = str_to_datetime(start_date, 'start')
	end_date_local = str_to_datetime(end_date, 'end')
	# Time difference in minutes between UTC and Local time
	timediff = abs(timezoneoffset)
	# Converting to UTC time
	if (timezoneoffset < 0):
		start_date_utc = start_date_local - timedelta(minutes=timediff)
		end_date_utc = end_date_local - timedelta(minutes=timediff)
	else:
		start_date_utc = start_date_local + timedelta(minutes=timediff)
		end_date_utc = end_date_local + timedelta(minutes=timediff)
	start_date_utc = start_date_utc.replace(tzinfo=pytz.UTC)
	end_date_utc = end_date_utc.replace(tzinfo=pytz.UTC)
	# Get all the data of analysis
	data = AnalysisData.objects.all().order_by('-pk')
	if (catalog=='ALL'):
		data = list(filter(lambda x: x.timestamp>=start_date_utc and x.timestamp<=end_date_utc and x.source.user==user, data))
	elif (catalog=='UNKNOWN'):
		data = list(filter(lambda x: x.timestamp>=start_date_utc and x.timestamp<=end_date_utc and x.source.user==user and x.person=='Unknown', data))
	else:
		catalog_obj = Catalog.objects.get(catalog_name=catalog, user_id=user)
		persons = Person.objects.filter(catalog=catalog_obj, user=user).values('uuid')
		person_list = []
		for person in persons:
			person_list.append(str(person['uuid']))
		data = list(filter(lambda x: x.timestamp>=start_date_utc and x.timestamp<=end_date_utc and x.source.user==user and (x.person in person_list), data))
	# Converting datetime start and end if None
	if (start_date==None):
		start_date = start_date_utc.strftime('%b %d, %Y %H:%M')
	if (end_date==None):
		end_date = end_date_utc.strftime('%b %d, %Y %H:%M')
	# Arrange data in required format as dataframe
	person_uuid = []
	person_thumbnail = []
	person_name = []
	person_catalog = []
	person_seen_count = []
	face_cropped = []
	face_timestamp = []
	face_source = []
	for d in data:
		if not (d.person in person_uuid):
			person_uuid.append(d.person)
			person_seen_count.append(1)
			face_cropped.append([d.cropped_face])
			face_timestamp.append([d.timestamp])
			face_source.append([d.source.source_name])
			if not (d.person == 'Unknown'):
				person_obj = Person.objects.get(uuid=d.person)
				person_faces_obj = PersonFace.objects.filter(person=person_obj)
				person_thumbnail.append(person_faces_obj[0].crop_face_image)
				person_name.append(person_obj.first_name + ' ' + person_obj.last_name)
				person_catalog.append(person_obj.catalog.catalog_name)
			else:
				person_thumbnail.append('training-faces/no_pic.jpg')
				person_name.append(' ')
				person_catalog.append('UNKNOWN')
		else:
			index = person_uuid.index(d.person)
			person_seen_count[index] = person_seen_count[index] + 1
			face_cropped[index].append(d.cropped_face)
			face_timestamp[index].append(d.timestamp)
			face_source[index].append(d.source.source_name)

	face_detail = zip(face_cropped, face_timestamp, face_source)
	f_detail = []
	for x, y, z in face_detail:
		f_detail.append(zip(x, y, z))

	dataframe = zip(person_uuid, person_thumbnail, person_name, person_catalog, person_seen_count)

	datadetail = zip(person_uuid, f_detail)

	context = {'catalogs':catalogs, 'start_date':start_date, 'end_date':end_date, 'catalog':catalog, 'dataframe':dataframe, 'datadetail':datadetail}
	return render(request, 'report/report.html', context)



def str_to_datetime(string, type):
	try:
		date_obj = datetime.strptime(string, '%b %d, %Y %H:%M')
	except:
		if type=='start':
			date_obj = datetime.now() - timedelta(days=10)
		else:
			date_obj = datetime.now()
	return date_obj