import json

from django.shortcuts import render, redirect
from django.contrib.auth.decorators import login_required

from recognition.models import Source, Person, PersonFace
from catalog.models import Catalog


@login_required(redirect_field_name='login')
def dashboard(request):
	user = request.user
	# Get cameras
	cameras = Source.objects.filter(user=user, source_type='live')
	# Get count of cameras for each status
	camera_active = list(filter(lambda x: x.source_status=='active', cameras))
	camera_inactive = list(filter(lambda x: x.source_status=='inactive', cameras))
	cam_count = len(cameras)
	cam_active_count = len(camera_active)
	cam_inactive_count = len(camera_inactive)
	# Get video files
	videos = Source.objects.filter(user=user, source_type='video_file')
	# Get count of processing status of video files
	video_pending = list(filter(lambda x: x.source_status=='pending', videos))
	video_processing = list(filter(lambda x: x.source_status=='processing', videos))
	video_complete = list(filter(lambda x: x.source_status=='complete', videos))
	video_count = len(videos)
	video_pending_count = len(video_pending)
	video_processing_count = len(video_processing)
	video_complete_count = len(video_complete)
	# Get total person added for recognition
	persons = Person.objects.filter(user=user)
	person_count = len(persons)
	faces = PersonFace.objects.all()
	person_faces = list(filter(lambda x: x.person.user==user, faces))
	face_count = len(person_faces)

	context = {
		'person_count':person_count,
		'face_count':face_count,
		'cam_active_count':cam_active_count, 
		'cam_inactive_count':cam_inactive_count, 
		'cam_count':cam_count,
		'video_count':video_count,
		'video_processing_count':video_processing_count,
		'video_pending_count':video_pending_count,
		'video_complete_count':video_complete_count}
	return render(request, 'registration/dashboard.html', context)


def login(request):
	return redirect('dashboard')