from imutils import paths
import imutils
import face_recognition
import pickle
import cv2
import os
import time
from datetime import datetime, timedelta
from sklearn.cluster import AffinityPropagation
import numpy as np

from cloudview.settings import BASE_DIR, MEDIA_URL

from .models import AnalysisData, Notification, NotificationHistory, Person, PersonFace
from .calculation import calculate_height
from .train_model import train_face

def video_analysis(video_source):
	# Change status of source to processing
	video_source.source_status = 'processing'
	video_source.save()
	# Get related notification
	notification = Notification.objects.get(related_uuid=video_source.uuid, notification_type='analysis')
	# Create notification message for process start
	notification_msg = NotificationHistory()
	notification_msg.status = 'processing'
	notification_msg.message = '[INFO] Analysis of Video File Started...'
	notification_msg.notification = notification
	notification_msg.save()
	# Load the necessary model
	user = video_source.user
	data = pickle.loads(open(BASE_DIR + MEDIA_URL + 'models/' + str(user.uuid) + '.pickle', "rb").read())
	# initialize the pointer to the video file and the video writer
	stream = cv2.VideoCapture(BASE_DIR + MEDIA_URL + str(video_source.video_file))
	# loop over frames from the video file stream
	fps_required = video_source.fps
	frame_no = 1
	nbFrames = stream.get(cv2.CAP_PROP_FRAME_COUNT)
	nbFps = stream.get(cv2.CAP_PROP_FPS)
	frame_increment = int(nbFps / fps_required)
	tolerance = 1 - (video_source.threshold / 200)
	total_frame_to_analyze = int(nbFrames/frame_increment)
	try:
		while True:
			# Initialize stream to read the required frame number
			try:
				stream.set(cv2.CAP_PROP_POS_FRAMES, frame_no)
			except:
				# Create notification message for frame processing
				notification_msg = NotificationHistory()
				notification_msg.status = 'complete'
				notification_msg.message = '[ERROR] Stream Position Error While Analyzing Video...'
				notification_msg.notification = notification
				notification_msg.save()
				break
			frame_no = frame_no + frame_increment
			time_second = stream.get(cv2.CAP_PROP_POS_MSEC) / 1000
			time_delta = timedelta(0, time_second)
			frame_timestamp = video_source.initial_timestamp + time_delta
			# grab the next frame
			(grabbed, frame) = stream.read()
			# if the frame was not grabbed, then we have reached the
			# end of the stream
			if not grabbed:
				# Create notification message for frame processing
				notification_msg = NotificationHistory()
				notification_msg.status = 'complete'
				notification_msg.message = '[ERROR] Frame Not Obtained While Analyzing Video...'
				notification_msg.notification = notification
				notification_msg.save()
				break
			# Create notification message for frame processing
			notification_msg = NotificationHistory()
			notification_msg.status = 'processing'
			notification_msg.message = '[INFO] Processing {} of {}'.format(int(frame_no/frame_increment), total_frame_to_analyze + 1)
			notification_msg.notification = notification
			notification_msg.save()
			# convert the input frame from BGR to RGB then resize it to have
			# a width of 750px (to speedup processing)
			rgb = cv2.cvtColor(frame, cv2.COLOR_BGR2RGB)
			# height = calculate_height(rgb.shape[0], rgb.shape[1])
			# rgb = cv2.resize(rgb, (500, height), interpolation=cv2.INTER_AREA)
			# detect the (x, y)-coordinates of the bounding boxes
			# corresponding to each face in the input frame, then compute
			# the facial embeddings for each face
			boxes = face_recognition.face_locations(rgb, model='cnn')
			encodings = face_recognition.face_encodings(rgb, boxes)
			names = []
			# loop over the facial embeddings
			for encoding in encodings:
				# attempt to match each face in the input image to our known
				# encodings
				matches = face_recognition.compare_faces(data["encodings"], encoding, tolerance=tolerance)
				name = "Unknown"
				# check to see if we have found a match
				if True in matches:
					# find the indexes of all matched faces then initialize a
					# dictionary to count the total number of times each face
					# was matched
					matchedIdxs = [i for (i, b) in enumerate(matches) if b]
					counts = {}
					# loop over the matched indexes and maintain a count for
					# each recognized face face
					for i in matchedIdxs:
						name = data["person"][i]
						counts[name] = counts.get(name, 0) + 1
					# determine the recognized face with the largest number
					# of votes (note: in the event of an unlikely tie Python
					# will select first entry in the dictionary)
					name = max(counts, key=counts.get)
				# update the list of names
				names.append(name)
			# loop over the recognized faces
			for ((top, right, bottom, left), name) in zip(boxes, names):
				# rescale the face coordinates
				# top = int(top * r)
				# right = int(right * r)
				# bottom = int(bottom * r)
				# left = int(left * r)
				# save analysis data to db
				analysis_data = AnalysisData()
				analysis_data.source = video_source
				analysis_data.person = name
				analysis_data.timestamp = frame_timestamp
				analysis_data.save()
				# save the cropped face in disk
				cropped_filepath = 'media/analysis_faces/{}/{}/{}.jpg'.format(user.uuid, video_source.uuid, analysis_data.uuid)
				cropped_image = frame[top:bottom, left:right]
				filedir = os.path.dirname(cropped_filepath)
				if not os.path.exists(filedir):
					os.makedirs(filedir)
				cv2.imwrite(cropped_filepath, cropped_image)
				analysis_data.cropped_face = cropped_filepath[6:]
				analysis_data.save()
		stream.release()
		# Change source status to complete
		video_source.source_status = 'complete'
		video_source.save()
		# Create notification message for frame processing
		notification_msg = NotificationHistory()
		notification_msg.status = 'complete'
		notification_msg.message = '[INFO] Face Recognition Completed Successfully...'
		notification_msg.notification = notification
		notification_msg.save()
	except Exception as e:
		# Create notification message for frame processing
		notification_msg = NotificationHistory()
		notification_msg.status = 'complete'
		notification_msg.message = '[ERROR] Error in Analyzing Video... {}'.format(e)
		notification_msg.notification = notification
		notification_msg.save()



def livestream(video_source):
	# Change status of source to processing
	video_source.source_status = 'active'
	video_source.save()
	# Get related notification
	notification = Notification.objects.get(related_uuid=video_source.uuid, notification_type='source')
	# Create notification message for process start
	notification_msg = NotificationHistory()
	notification_msg.status = 'processing'
	notification_msg.message = '[INFO] Analysis of Live Stream Started... Cam URL - {}'.format(video_source.camera_url)
	notification_msg.notification = notification
	notification_msg.save()
	# Load the necessary model
	user = video_source.user
	data = pickle.loads(open(BASE_DIR + MEDIA_URL + 'models/' + str(user.uuid) + '.pickle', "rb").read())
	# initialize the pointer to the video file and the video writer
	if (video_source.camera_url == '0'):
		stream = cv2.VideoCapture(int(video_source.camera_url))
	else:
		stream = cv2.VideoCapture(str(video_source.camera_url))
	# loop over frames from the video file stream
	fps_required = video_source.fps
	frame_no = 1
	next_frame = 1
	nbFps = stream.get(cv2.CAP_PROP_FPS)
	frame_increment = int(nbFps / fps_required)
	tolerance = 1 - (video_source.threshold / 200)
	try:
		while True:
			# grab the next frame
			if (video_source.source_status == 'inactive'):
				break
			if (frame_no == next_frame):
				(grabbed, frame) = stream.read()
				timestamp = datetime.now()
				# if the frame was not grabbed, then we have reached the
				# end of the stream
				if not grabbed:
					# Create notification message for frame processing
					notification_msg = NotificationHistory()
					notification_msg.status = 'inactive'
					notification_msg.message = '[ERROR] Frame Not Obtained While Analyzing Video...'
					notification_msg.notification = notification
					notification_msg.save()
					break
				# Create notification message for frame processing
				notification_msg = NotificationHistory()
				notification_msg.status = 'processing'
				notification_msg.message = 'Analyzing and Processing Frame'
				notification_msg.notification = notification
				notification_msg.save()
				# convert the input frame from BGR to RGB then resize it to have
				# a width of 750px (to speedup processing)
				rgb = cv2.cvtColor(frame, cv2.COLOR_BGR2RGB)
				# height = calculate_height(rgb.shape[0], rgb.shape[1])
				# rgb = cv2.resize(rgb, (500, height), interpolation=cv2.INTER_AREA)
				# detect the (x, y)-coordinates of the bounding boxes
				# corresponding to each face in the input frame, then compute
				# the facial embeddings for each face
				boxes = face_recognition.face_locations(rgb, model='cnn')
				encodings = face_recognition.face_encodings(rgb, boxes)
				names = []
				# loop over the facial embeddings
				for encoding in encodings:
					# attempt to match each face in the input image to our known
					# encodings
					matches = face_recognition.compare_faces(data["encodings"], encoding, tolerance=tolerance)
					name = "Unknown"
					# check to see if we have found a match
					if True in matches:
						# find the indexes of all matched faces then initialize a
						# dictionary to count the total number of times each face
						# was matched
						matchedIdxs = [i for (i, b) in enumerate(matches) if b]
						counts = {}
						# loop over the matched indexes and maintain a count for
						# each recognized face face
						for i in matchedIdxs:
							name = data["person"][i]
							counts[name] = counts.get(name, 0) + 1
						# determine the recognized face with the largest number
						# of votes (note: in the event of an unlikely tie Python
						# will select first entry in the dictionary)
						name = max(counts, key=counts.get)
					# update the list of names
					names.append(name)
				# loop over the recognized faces
				for ((top, right, bottom, left), name) in zip(boxes, names):
					# rescale the face coordinates
					# top = int(top * r)
					# right = int(right * r)
					# bottom = int(bottom * r)
					# left = int(left * r)
					# save analysis data to db
					analysis_data = AnalysisData()
					analysis_data.source = video_source
					analysis_data.person = name
					analysis_data.timestamp = timestamp
					analysis_data.save()
					# save the cropped face in disk
					cropped_filepath = 'media/analysis_faces/{}/{}/{}.jpg'.format(user.uuid, video_source.uuid, analysis_data.uuid)
					cropped_image = frame[top:bottom, left:right]
					filedir = os.path.dirname(cropped_filepath)
					if not os.path.exists(filedir):
						os.makedirs(filedir)
					cv2.imwrite(cropped_filepath, cropped_image)
					analysis_data.cropped_face = cropped_filepath[6:]
					analysis_data.save()
				next_frame = next_frame + frame_increment
			else:
				pass
			frame_no = frame_no + 1
			print ('Frame {} - Next Frame {}'.format(frame_no, next_frame))
		stream.release()
		# Change source status to complete
		video_source.source_status = 'inactive'
		video_source.save()
		# Create notification message for frame processing
		notification_msg = NotificationHistory()
		notification_msg.status = 'complete'
		notification_msg.message = '[INFO] Face Recognition Completed Successfully... Camera is inactive'
		notification_msg.notification = notification
		notification_msg.save()
	except Exception as e:
		video_source.source_status = 'inactive'
		video_source.save()
		# Create notification message for frame processing
		notification_msg = NotificationHistory()
		notification_msg.status = 'complete'
		notification_msg.message = '[ERROR] Error in Analyzing Video... {}'.format(e)
		notification_msg.notification = notification
		notification_msg.save()


def cluster(user_uuid):
	encoding_path = BASE_DIR + MEDIA_URL + 'models/' + user_uuid + '/unknown.pickle'
	with open(encoding_path, 'rb') as file:
		data = pickle.load(file)
	data = np.array(data)
	encodings = [d['encoding'] for d in data]

	clt = AffinityPropagation()
	clt.fit(encodings)
	# determine the total number of unique faces found in the dataset
	labelIDs = np.unique(clt.labels_)
	for labelID in labelIDs:
		# find all indexes into the `data` array that belong to the
		# current label ID, then randomly sample a maximum of 25 indexes
		# from the set
		idxs = np.where(clt.labels_ == labelID)[0]
		idxs = np.random.choice(idxs, size=min(1000, len(idxs)), replace=False)
		# Create a new person instance
		new_person = Person()
		new_person.user = User.objects.get(uuid=user_uuid)
		new_person.save()
		# loop over the sampled indexes
		for i in idxs:
			face_uuid = data[i]["uuid"]
			face_path = data[i]["face_path"]
			filename = face_path.split('/')[-1]
			training_image_path = 'training-dataset/{}/{}/{}'.format(user_uuid, new_person.uuid, filename)
			shutil.copy(face_path, BASE_DIR + MEDIA_URL + training_image_path)
			new_face = PersonFace()
			new_face.image_name = training_image_path
			new_face.person = new_person
			new_face.save()
		train_face.delay(new_person.uuid)