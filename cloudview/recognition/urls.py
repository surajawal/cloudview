from django.urls import path

from . import views

urlpatterns = [
	path('', views.person_list, name='person_list'),
	path('new-person', views.add_person, name='add_person'),
	path('new-person/<person_uuid>/contact', views.add_person_contact, name='add_person_contact'),
	path('new-person/<person_uuid>/images', views.add_person_images, name='add_person_images'),
	path('train/<person_uuid>', views.train_face, name='train_face'),
	path('delete-person/<person_uuid>', views.delete_person, name='delete_person'),
	path('select-source', views.analysis_index, name='analysis_index'),
	path('analysis/<source_type>', views.analysis_form, name='analysis_form'),
	path('manage-camera', views.manage_camera, name='manage_camera'),
	path('manage-video', views.manage_video, name='manage_video'),
	path('toggle-camera-status/<source_uuid>', views.toggle_camera_status, name='toggle_camera_status'),
	path('logs/<notification_type>/<related_uuid>', views.view_logs, name='view_logs'),
]