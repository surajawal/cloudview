from django.db import models
import uuid
from datetime import datetime

from django_countries.fields import CountryField

from catalog.models import Catalog
from user_authentication.models import User


def person_face_upload(instance, filename):
	filename = filename.split('/')
	filename = filename[len(filename)-1]
	return ('training-dataset/{}/{}/{}'.format(instance.person.user.uuid, instance.person.uuid, filename))


def video_file_upload(instance, filename):
	filename = filename.split('/')
	filename = filename[-1]
	return ('analysis-video/{}/{}'.format(instance.user.uuid, filename))


# Create your models here.
class Person(models.Model):
	GENDER_CHOICES = (
		('MALE', 'MALE'),
		('FEMALE', 'FEMALE'),
	)

	uuid = models.UUIDField(null=True, unique=True, default=uuid.uuid4, editable=False)
	first_name = models.CharField(max_length=100, null=True, blank=True)
	middle_name = models.CharField(max_length=100, null=True, blank=True)
	last_name = models.CharField(max_length=100, null=True, blank=True)
	gender = models.CharField(max_length=10, null=True, blank=True, choices=GENDER_CHOICES)
	email = models.EmailField(max_length=255, blank=True, null=True)
	phone_number = models.CharField(max_length=20, blank=True, null=True)
	street = models.CharField(max_length=100, blank=True, null=True)
	city = models.CharField(max_length=100, blank=True, null=True)
	state = models.CharField(max_length=100, blank=True, null=True)
	country = CountryField(blank=True, null=True, default='US')
	postal_code = models.IntegerField(blank=True, null=True)
	description = models.TextField(max_length=1000, blank=True, null=True)
	catalog = models.ForeignKey(Catalog, on_delete=models.CASCADE, blank=True, null=True)
	user = models.ForeignKey(User, on_delete=models.CASCADE, blank=True, null=True)

	def __str__(self):
		try:
			return str(self.catalog.catalog_name) + " - " + str(self.first_name)
		except:
			return "Undefined - " + str(self.first_name)


class PersonFace(models.Model):
	uuid = models.UUIDField(null=True, unique=True, default=uuid.uuid4, editable=False)
	image_name = models.ImageField(upload_to=person_face_upload, max_length=500)
	train_status = models.BooleanField(default=False)
	crop_face_image = models.CharField(max_length=500, blank=True, null=True)
	person = models.ForeignKey(Person, on_delete=models.CASCADE, blank=True, null=True)

	def __str__(self):
		return str(self.image_name)


class Notification(models.Model):
	NOTIFICATION_TYPE_CHOICES = (
		('train', 'TRAIN STATUS'),
		('analysis', 'ANALYSIS STATUS'),
		('source', 'SOURCE STATUS'),
	)
	uuid = models.UUIDField(null=True, unique=True, default=uuid.uuid4, editable=False)
	notification_type = models.CharField(max_length=100, choices=NOTIFICATION_TYPE_CHOICES)
	created_at = models.DateTimeField(auto_now_add=True)
	related_uuid = models.UUIDField(null=True, unique=False, editable=False)
	user = models.ForeignKey(User, on_delete=models.CASCADE, blank=True, null=True)


class NotificationHistory(models.Model):
	NOTIFICATION_STATUS_CHOICES = (
		('pending', 'PENDING'),
		('processing', 'PROCESSING'),
		('complete', 'COMPLETED'),
		('active', 'ACTIVE'),
		('inactive', 'INACTIVE'),
	)
	uuid = models.UUIDField(null=True, unique=True, default=uuid.uuid4, editable=False)
	status = models.CharField(max_length=100, choices=NOTIFICATION_STATUS_CHOICES)
	message = models.TextField()
	last_modified = models.DateTimeField(auto_now=True)
	notification = models.ForeignKey(Notification, on_delete=models.CASCADE)


class Source(models.Model):
	SOURCE_TYPE_CHOICES = (
		('video_file', 'VIDEO FILES'),
		('live', 'LIVE STREAM'),
	)
	SOURCE_STATUS_CHOICES = (
		('pending', 'PENDING'),
		('processing', 'PROCESSING'),
		('complete', 'COMPLETED'),
		('active', 'ACTIVE'),
		('inactive', 'INACTIVE'),
	)
	THRESHOLD_CHOICES = (
		(80, 80),
		(60, 60),
		(40, 40),
	)
	uuid = models.UUIDField(null=True, unique=True, default=uuid.uuid4, editable=False)
	source_type = models.CharField(max_length=100, choices=SOURCE_TYPE_CHOICES, blank=True, null=True)
	source_status = models.CharField(max_length=100, choices=SOURCE_STATUS_CHOICES, default='pending')
	source_name = models.CharField(max_length=500)
	video_file = models.FileField(upload_to=video_file_upload, blank=True, null=True)
	initial_timestamp = models.DateTimeField(default=datetime.now(), blank=True, null=True)
	camera_url = models.CharField(blank=True, null=True, max_length=500)
	threshold = models.IntegerField(choices=THRESHOLD_CHOICES, default=THRESHOLD_CHOICES[0])
	fps = models.IntegerField(default=5)
	user = models.ForeignKey(User, on_delete=models.CASCADE, blank=True, null=True)

	def __str__(self):
		return self.source_name


class AnalysisData(models.Model):
	uuid = models.UUIDField(null=True, unique=True, default=uuid.uuid4, editable=False, blank=True)
	timestamp = models.DateTimeField()
	source = models.ForeignKey(Source, on_delete=models.CASCADE)
	person = models.CharField(max_length=100)
	cropped_face = models.CharField(max_length=500, blank=True, null=True)


