from django.contrib import admin

from .models import Person, PersonFace, Notification, NotificationHistory, Source, AnalysisData

# Register your models here.
class PersonFaceInline(admin.TabularInline):
	model = PersonFace


class PersonAdmin(admin.ModelAdmin):
	inlines = [
		PersonFaceInline,
	]


class NotificationHistoryInline(admin.TabularInline):
	model = NotificationHistory


class NotificationAdmin(admin.ModelAdmin):
	inlines = [
		NotificationHistoryInline,
	]


class AnalysisDataInline(admin.TabularInline):
	model = AnalysisData


class SourceAdmin(admin.ModelAdmin):
	inlines = [
		AnalysisDataInline,
	]


admin.site.register(Person, PersonAdmin)
admin.site.register(Notification, NotificationAdmin)
admin.site.register(Source, SourceAdmin)