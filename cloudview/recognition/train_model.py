from imutils import paths
import face_recognition
import pickle
import cv2
import os
from datetime import datetime
from django.conf import settings

from .models import PersonFace, Person, Notification, NotificationHistory

from .calculation import calculate_height	

def train_face(person_uuid):
	# Getting the related notification object
	notification = Notification.objects.get(notification_type='train', related_uuid=person_uuid)
	# Getting the related person object
	person = Person.objects.get(uuid=person_uuid)
	# Creating notification history for the notification
	notification_msg = NotificationHistory(status='processing', notification=notification, message='[INFO] Training for face recognition started')
	notification_msg.save()
	# Getting images for training of the person that are not trained already
	images = PersonFace.objects.filter(person=person, train_status=False)
	# initialize the list of known encodings and known names
	knownEncodings = []
	imageId = []
	personId = []
	for (i, imagePath) in enumerate(images):
		image = imagePath.image_name
		# Creating notification history for the notification
		notification_msg = NotificationHistory(status='processing', notification=notification, message='[INFO] Encoding image {} of {}'.format(i+1, len(images)))
		notification_msg.save()
		# load the input image and convert it from RGB (OpenCV ordering)
		# to dlib ordering (RGB)
		image = cv2.imread(settings.BASE_DIR + settings.MEDIA_URL + str(image))
		rgb = cv2.cvtColor(image, cv2.COLOR_BGR2RGB)
		# Resize image to 500px width
		# height = calculate_height(rgb.shape[0], rgb.shape[1])
		# rgb = cv2.resize(rgb, (500, height), interpolation=cv2.INTER_AREA)
		# detect the (x, y)-coordinates of the bounding boxes
		# corresponding to each face in the input image
		boxes = face_recognition.face_locations(rgb, model='cnn')
		# Get the facial bounding box
		# x1 = boxes[0][0]
		# y1 = boxes[0][3]
		# x2 = boxes[0][2]
		# y2 = boxes[0][1]
		# Enlarge the bounding box of the face if possible by 30 px
		x1 = boxes[0][0] if boxes[0][0]<30 else boxes[0][0]-30
		y1 = boxes[0][3] if boxes[0][3]<30 else boxes[0][3]-30
		x2 = boxes[0][2] if boxes[0][2]>image.shape[1]-30 else boxes[0][2]+30
		y2 = boxes[0][1] if boxes[0][1]>image.shape[0]-30 else boxes[0][1]+30
		# Crop the face from image
		crop_face = image[x1:x2, y1:y2]
		filepath = 'media/training-faces' + str(imagePath.image_name)[16:]
		filedir = os.path.dirname(filepath)
		if not os.path.exists(filedir):
			os.makedirs(filedir)
		cv2.imwrite(filepath, crop_face)
		# compute the facial embedding for the face
		encodings = face_recognition.face_encodings(rgb, boxes)
		# loop over the encodings
		for encoding in encodings:
			# add each encoding + name to our set of known names and
			# encodings
			knownEncodings.append(encoding)
			imageId.append(imagePath.uuid)
			personId.append(person_uuid)
		# Change status and add crop face into the database
		imagePath.train_status = True
		imagePath.crop_face_image = filepath[6:]
		imagePath.save()
	if (len(images) == 0):
		# Creating notification history for the notification
		notification_msg = NotificationHistory(status='processing', notification=notification, message='[INFO] No Images to Train')
		notification_msg.save()
	else:
		# Creating notification history for the notification
		notification_msg = NotificationHistory(status='processing', notification=notification, message='[INFO] Creating/Updating model')
		notification_msg.save()
		model_directory = settings.BASE_DIR + settings.MEDIA_URL + 'models'
		model_name = str(person.user.uuid) + '.pickle'
		if not os.path.isdir(model_directory):
			os.mkdir(model_directory)
		if not os.path.exists(model_directory + '/' + model_name):
			data = {'encodings': knownEncodings, 'person': personId, 'image': imageId}
		else:
			with open(model_directory + '/' + model_name, 'rb') as file:
				data = pickle.load(file)
			for i in range(len(knownEncodings)):
				data['encodings'].append(knownEncodings[i])
				data['person'].append(personId[i])
				data['image'].append(imageId[i])
		with open(model_directory + '/' + model_name, 'wb') as file:
			file.write(pickle.dumps(data))
