from celery import shared_task
from imutils import paths
import face_recognition
import pickle
import cv2
import os
from datetime import datetime
from cloudview.settings import BASE_DIR, MEDIA_URL

from .models import PersonFace, Person, Notification, NotificationHistory, Source

from .train_model import train_face as tf
from .analysis import video_analysis as va, cluster as clus, livestream as ls

@shared_task
def train_face(person_uuid):
	notification = Notification.objects.get(notification_type='train', related_uuid=person_uuid)
	try:
		tf(person_uuid)
		# Creating notification history for the notification
		notification_msg = NotificationHistory(status='complete', notification=notification, message='[INFO] Successfully added person for recognition')
		notification_msg.save()
	except Exception as e:
		# Creating notification history for the notification
		notification_msg = NotificationHistory(status='complete', notification=notification, message='[ERROR] {}'.format(e))
		notification_msg.save()


@shared_task
def video_analysis(video_uuid):
	notification = Notification.objects.get(related_uuid=video_uuid, notification_type='analysis')
	video_source = Source.objects.get(uuid=video_uuid)
	try:
		va(video_source)
	except Exception as e:
		notification_msg = NotificationHistory(status='complete', notification=notification, message='[ERROR] {}'.format(e))


@shared_task
def livestream_analysis(video_uuid):
	notification = Notification.objects.get(related_uuid=video_uuid, notification_type='source')
	video_source = Source.objects.get(uuid=video_uuid)
	try:
		ls(video_source)
	except Exception as e:
		# Change source status to complete
		video_source.source_status = 'inactive'
		video_source.save()
		# Create notification message for frame processing
		notification_msg = NotificationHistory()
		notification_msg.status = 'complete'
		notification_msg.message = '[ERROR] Error has occured... {}'.format(e)
		notification_msg.notification = notification
		notification_msg.save()