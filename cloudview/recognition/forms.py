from django import forms
from .models import Person, PersonFace, Source

class PersonCreationForm(forms.ModelForm):
	class Meta:
		model = Person
		fields = ['first_name', 'middle_name', 'last_name', 'gender', 'email', 'phone_number', 'street', 'city', 'state', 'country', 'postal_code', 'description']


class PersonFaceCreationForm(forms.ModelForm):
	class Meta:
		model = PersonFace
		fields = ['image_name',]


class SourceForm(forms.ModelForm):
	class Meta:
		model = Source
		fields = ['source_name', 'video_file', 'initial_timestamp', 'camera_url', 'threshold', 'fps']