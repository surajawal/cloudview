from imutils import paths
import face_recognition
import pickle
import cv2
import os
from datetime import datetime
from cloudview.settings import BASE_DIR, MEDIA_URL

from django.shortcuts import render, redirect
from django.contrib.auth.decorators import login_required
from django.http import Http404, HttpResponseRedirect
from django.urls import reverse

from .models import Person, PersonFace, Notification, NotificationHistory, Source
from .forms import PersonCreationForm, SourceForm, PersonFaceCreationForm
from catalog.models import Catalog

from .tasks import train_face as training
from .tasks import video_analysis, livestream_analysis

# Create your views here.
@login_required(redirect_field_name='login')
def person_list(request):
	user = request.user
	persons = Person.objects.filter(user=user).order_by('catalog')
	thumbnails = []
	for person in persons:
		face = PersonFace.objects.filter(person=person, train_status=True)
		try:
			thumbnails.append(face[0].crop_face_image)
		except:
			thumbnails.append('no_pic.jpg')
	personList = zip(persons, thumbnails)
	context = {'persons':personList}
	return render(request, 'recognition/person_list.html', context)


@login_required(redirect_field_name='login')
def add_person(request):
	user = request.user
	catalogs = Catalog.objects.filter(user_id=user)
	if (request.method == 'POST'):
		form = PersonCreationForm(request.POST)
		if form.is_valid():
			new_person = Person()
			new_person.user = user
			first_name = form.cleaned_data['first_name']
			middle_name = form.cleaned_data['middle_name']
			last_name = form.cleaned_data['last_name']
			if (first_name is not None):
				new_person.first_name = first_name.upper()
			if (middle_name is not None):
				new_person.middle_name = middle_name.upper()
			if (last_name is not None):
				new_person.last_name = last_name.upper()
			new_person.gender = form.cleaned_data['gender']
			new_person.email = form.cleaned_data['email']
			try:
				catalog_uuid = request.POST['catalog']
				new_person.catalog = Catalog.objects.get(uuid=catalog_uuid, user_id=user)
			except:
				pass
			new_person.save()
			# Creating notification type train for given person uuid
			notification = Notification()
			notification.notification_type = 'train'
			notification.related_uuid = new_person.uuid
			notification.user = user
			notification.save()
			# Creating notification history for the notification
			notification_msg = NotificationHistory(status='pending', notification=notification, message='[INFO] Successfully created person and added personal details.')
			notification_msg.save()
			return redirect('add_person_contact', new_person.uuid)
	else:
		form = PersonCreationForm()
	context = {'form':form, 'catalogs':catalogs}
	return render(request, 'recognition/add_person.html', context)

@login_required(redirect_field_name='login')
def add_person_contact(request, person_uuid):
	user = request.user
	if (request.method == 'POST'):
		form = PersonCreationForm(request.POST)
		if form.is_valid():
			new_person = Person.objects.get(uuid=person_uuid)
			new_person.phone_number = form.cleaned_data['phone_number']
			new_person.street = form.cleaned_data['street']
			new_person.city = form.cleaned_data['city']
			new_person.state = form.cleaned_data['state']
			new_person.country = form.cleaned_data['country']
			new_person.postal_code = form.cleaned_data['postal_code']
			new_person.description = form.cleaned_data['description']
			new_person.save()
			# Creating notification history
			notification = Notification.objects.get(related_uuid=person_uuid)
			notification_msg = NotificationHistory(status='pending', notification=notification, message='[INFO] Successfully added contact details.')
			notification_msg.save()
			return redirect('add_person_images', new_person.uuid)
	else:
		form = PersonCreationForm()
	context = {'form':form}
	return render(request, 'recognition/add_person_contact.html', context)

@login_required(redirect_field_name='login')
def add_person_images(request, person_uuid):
	user = request.user
	person = Person.objects.get(uuid=person_uuid)
	if (request.method == 'POST'):
		form = PersonFaceCreationForm(request.POST, request.FILES)
		if form.is_valid():
			new_image = PersonFace()
			new_image.person = person
			new_image.image_name = request.FILES['image_name']
			new_image.save()
			print (request.FILES['image_name'])
			# Creating notification history
			notification = Notification.objects.get(related_uuid=person_uuid)
			notification_msg = NotificationHistory(status='pending', notification=notification, message='[INFO] Successfully added image : {}.'.format(new_image.image_name))
			notification_msg.save()
			return HttpResponseRedirect(reverse('add_person_images', args=[person_uuid]))
		else:
			print (form.errors)
	else:
		form = PersonFaceCreationForm()
	context = {'form':form, 'person_uuid':person_uuid}
	return render(request, 'recognition/add_person_images.html', context)



@login_required(redirect_field_name='login')
def train_face(request, person_uuid):
	# Getting the related notification object
	try:
		notification = Notification.objects.get(notification_type='train', related_uuid=person_uuid)
	except:
		# Creating notification type train for given person uuid
		notification = Notification()
		notification.notification_type = 'train'
		notification.related_uuid = person_uuid
		notification.user = user
		notification.save()
	user = request.user
	person = Person.objects.get(uuid=person_uuid)
	training.delay(person_uuid)
	return redirect('person_list')


@login_required(redirect_field_name='login')
def delete_person(request, person_uuid):
	user = request.user
	person = Person.objects.get(uuid=person_uuid)
	if (person.user == user):
		person.delete()
	else:
		raise Http404("The person does not exist...")
	return redirect('person_list')



@login_required(redirect_field_name='login')
def analysis_index(request):
	context = {}
	return render(request, 'recognition/analysis_index.html', context)


@login_required(redirect_field_name='login')
def analysis_form(request, source_type):
	user = request.user
	if (source_type == 'live'):
		st = True
	elif (source_type == 'video_file'):
		st = False
	if (request.method == 'POST'):
		form = SourceForm(request.POST, request.FILES)
		if form.is_valid():
			new_source = Source()
			new_source.source_type = source_type
			new_source.source_name = form.cleaned_data['source_name']
			new_source.threshold = form.cleaned_data['threshold']
			new_source.fps = form.cleaned_data['fps']
			new_source.user = user
			if (source_type == 'video_file'):
				new_source.video_file = request.FILES['video_file']
				new_source.initial_timestamp = form.cleaned_data['initial_timestamp']
			elif (source_type == 'live'):
				new_source.camera_url = form.cleaned_data['camera_url']
				new_source.initial_timestamp = datetime.now()
			new_source.save()
			if (source_type == 'video_file'):
				notification = Notification()
				notification.notification_type = 'analysis'
				notification.related_uuid = new_source.uuid
				notification.user = user
				notification.save()
				video_analysis.delay(new_source.uuid)
			elif (source_type == 'live'):
				new_source.source_status = 'inactive'
				new_source.save()
				notification = Notification()
				notification.notification_type = 'source'
				notification.related_uuid = new_source.uuid
				notification.user = user
				notification.save()
				livestream_analysis(new_source.uuid)
			return redirect('manage_camera')
		else:
			print (form.errors)
	else:
		form = SourceForm()
	context = {'form':form, 'source_type':st}
	return render(request, 'recognition/analysis.html', context)


@login_required(redirect_field_name='login')
def manage_camera(request):
	user = request.user
	sources = Source.objects.filter(user=user, source_type='live')
	context = {'sources':sources}
	return render(request, 'recognition/manage_camera.html', context)


@login_required(redirect_field_name='login')
def manage_video(request):
	user = request.user
	videos = Source.objects.filter(user=user, source_type='video_file')
	context = {'videos':videos}
	return render(request, 'recognition/manage_video.html', context)


@login_required(redirect_field_name='login')
def toggle_camera_status(request, source_uuid):
	source = Source.objects.get(uuid=source_uuid)
	if (source.source_status == 'active'):
		source.source_status = 'inactive'
	else:
		source.source_status = 'active'
		livestream_analysis(source.uuid)
	source.save()
	return redirect('manage_camera')


@login_required(redirect_field_name='login')
def view_logs(request, notification_type, related_uuid):
	user = request.user
	notification = Notification.objects.get(user=user, notification_type=notification_type, related_uuid=related_uuid)
	logs = NotificationHistory.objects.filter(notification=notification).order_by('-pk')
	context = {'notification':notification, 'logs':logs}
	return render(request, 'recognition/logs.html', context)