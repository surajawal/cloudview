from django.urls import path

from . import views

urlpatterns = [
	path('', views.add_catalog, name='add_catalog'),
	path('<catalog_uuid>/', views.delete_catalog, name='delete_catalog')
]