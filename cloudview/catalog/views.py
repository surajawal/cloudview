from django.shortcuts import render, redirect
from django.http import Http404
from django.contrib.auth.decorators import login_required

from .forms import CatalogCreationForm
from .models import Catalog

# Create your views here.
@login_required(login_url='/login')
def add_catalog(request):
	user = request.user
	catalogs = Catalog.objects.filter(user_id=user)
	catalog_names = []
	for catalog in catalogs:
		catalog_names.append(catalog.catalog_name)
	if request.method == 'POST':
		catalog_form = CatalogCreationForm(request.POST)
		if catalog_form.is_valid():
			new_catalog = Catalog()
			new_catalog.user_id = user
			catalog_name = catalog_form.cleaned_data['catalog_name'].upper()
			new_catalog.catalog_name = catalog_name
			if not catalog_name in catalog_names:
				new_catalog.save()
				catalog_form = CatalogCreationForm()
			else:
				catalog_form.add_error('catalog_name', 'Catalog Already Exists')
	else:
		catalog_form = CatalogCreationForm()
	catalogs = Catalog.objects.filter(user_id=user).order_by('catalog_name')
	context = {'form': catalog_form, 'catalogs': catalogs}
	return render(request, 'catalog/add_catalog.html', context)


@login_required(login_url='/login')
def delete_catalog(request, catalog_uuid):
	user = request.user
	try:
		catalog = Catalog.objects.get(uuid=catalog_uuid)
		catalog.delete()
	except:
		raise Http404("The catalog does not exist...")
	return redirect('add_catalog')