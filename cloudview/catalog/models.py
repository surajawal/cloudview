from django.db import models

import uuid

from user_authentication.models import User

# Create your models here.
class Catalog(models.Model):
	uuid = models.UUIDField(null=True, unique=True, default=uuid.uuid4, editable=False)
	catalog_name = models.CharField(max_length=100, verbose_name='Catalog Name')
	user_id = models.ForeignKey(User, on_delete=models.CASCADE)

	def __str__(self):
		return str(self.catalog_name)