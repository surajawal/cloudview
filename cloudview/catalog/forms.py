from django import forms
from .models import Catalog

class CatalogCreationForm(forms.ModelForm):
	class Meta:
		model = Catalog
		fields = ['catalog_name',]