from django.shortcuts import render, redirect

from .admin import UserCreationForm

# Controller for user registration
def user_registration(request):
	if request.user.is_authenticated:
		return redirect('dashboard')
	else:
		args = {}
		if request.method == 'POST':
			form = UserCreationForm(request.POST)
			if form.is_valid():
				try:
					form.password2_match()
					user = form.save(commit=False)
					user.is_active = True
					user.save()
					return redirect('login')
				except:
					form.add_error('password2', 'Password does not match')
		else:
			form = UserCreationForm()
			args = {'form': form}
	return render(request, 'registration/registration.html', {'form':form})

# Controller for homepage or dashboard
def dashboard(request):
	if request.user.is_authenticated:
		return render(request, 'registration/dashboard.html', {})
	else:
		return redirect('login')

