from django import forms
from django.contrib import admin
from django.contrib.auth.models import Group
from django.contrib.auth.admin import UserAdmin as BaseUserAdmin
from django.contrib.auth.forms import ReadOnlyPasswordHashField

from .models import User

# Register your models here.
class UserCreationForm(forms.ModelForm):
	def __init__(self, *args, **kwargs):
		super(UserCreationForm, self).__init__(*args, **kwargs)
		self.fields['email'].label = 'Email'
		self.fields['first_name'].label = 'First Name'
		self.fields['middle_name'].label = 'Middle Name'
		self.fields['last_name'].label = 'Last Name'

	password1 = forms.CharField(label = 'Password', widget = forms.PasswordInput)
	password2 = forms.CharField(label = 'Password Confirmation', widget = forms.PasswordInput)

	class Meta:
		model = User
		fields = ('email', 'first_name', 'middle_name', 'last_name')

	def password2_match(self):
		password1 = self.cleaned_data.get("password1")
		password2 = self.cleaned_data.get("password2")
		if password1 and password2 and password1 != password2:
			#self.add_error('password2', 'Password does not match')
			raise forms.ValidationError("Password does not match")
		return password2

	def save(self, commit=True):
		user = super().save(commit = False)
		user.set_password(self.cleaned_data["password1"])
		if commit:
			user.save()
		return user


class UserChangeForm(forms.ModelForm):
	password = ReadOnlyPasswordHashField()

	class Meta:
		model = User
		fields = ('email', 'account_type', 'company_name', 'street', 'city', 'state', 'country', 'postal_code', 'first_name', 
			'middle_name', 'last_name', 'phone_number', 'password', 'is_active', 'is_admin')

	def clean_password(self):
		return self.initial["password"]


class UserEditForm(forms.ModelForm):
	def __init__(self, *args, **kwargs):
		super(UserEditForm, self).__init__(*args, **kwargs)
		self.fields['account_type'].label = 'Account Type'
		self.fields['company_name'].label = 'Company Name'
		self.fields['street'].label = 'Street'
		self.fields['city'].label = 'City'
		self.fields['state'].label = 'State'
		self.fields['country'].label = 'Country'
		self.fields['postal_code'].label = 'Postal Code'
		self.fields['first_name'].label = 'First Name'
		self.fields['middle_name'].label = 'Middle Name'
		self.fields['last_name'].label = 'Last Name'
		self.fields['phone_number'].label = 'Phone'


	class Meta:
		model = User
		fields = ('is_active', 'account_type', 'company_name', 'street', 'city', 'state', 'country', 'postal_code', 'first_name', 
			'middle_name', 'last_name', 'phone_number')


class UserAdmin(BaseUserAdmin):
	form = UserChangeForm
	add_form = UserCreationForm
	model = User
	list_display = ('is_admin', 'email', 'account_type', 'company_name', 'street', 'city', 'state', 'country', 'postal_code', 'first_name', 
	 	'middle_name', 'last_name', 'phone_number')
	list_filter = ('is_admin', 'account_type', 'country',)
	fieldsets = (
		(None, {'fields' : ('email', 'password',)}), 
		('Contact Person', {'fields': ('first_name', 'middle_name', 'last_name',)}), 
		('Address', {'fields':('street', 'city', 'state', 'country', 'postal_code',)}),
		('Contact', {'fields':('company_name', 'phone_number',)}), 
		('Permissions', {'fields': ('is_admin',)}),)
	add_fieldsets = ((None, {'classes': ('wide',),'fields': ('email', 'account_type', 'company_name', 'street', 'city', 'state', 'country', 'postal_code', 'first_name', 'middle_name', 'last_name', 'phone_number', 'password1', 'password2',)}),)
	search_fields = ('email',)
	ordering = ('email',)
	filter_horizontal = ()

admin.site.register(User, UserAdmin)

admin.site.unregister(Group)
