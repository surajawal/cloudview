import uuid

from django.db import models
from django.contrib.auth.models import AbstractUser, BaseUserManager, AbstractBaseUser
from django.core.validators import MaxValueValidator, MinValueValidator

from datetime import date

from django.conf import settings


from django_countries.fields import CountryField


class UserManager(BaseUserManager):
	def create_user(self, email, password, uuid, account_type, company_name, street, city, state, country, postal_code, first_name, middle_name, last_name, phone_number):
		if not email:
			raise ValueError('Users must enter a valid email address')

		user = self.model(
			email = self.normalize_email(email), 
			uuid=uuid, 
			account_type=account_type, 
			company_name=company_name, 
			street=street, 
			city=city, 
			state=state, 
			country=country, 
			postal_code=postal_code,
			first_name=first_name, 
			middle_name=middle_name, 
			last_name=last_name,
			phone_number=phone_number,
		)
		user.set_password(password)
		user.save(using=self._db)
		return user

	def create_superuser(self, email, password, company_name, uuid, account_type, street, city, state, country, postal_code, first_name, middle_name, last_name, phone_number):
		user = self.create_user(email=email, password=password, company_name=company_name, uuid=uuid, account_type=account_type, street=street, city=city, state=state, country=country, postal_code=postal_code, first_name=first_name, middle_name=middle_name, last_name=last_name, phone_number=phone_number)
		user.is_admin = True
		user.save(using=self._db)
		return user


# Create your models here.
class User(AbstractBaseUser):
	ACCOUNT_TYPE_CHOICES = (
		('individual', 'INDIVIDUAL'),
		('company', 'COMPANY'),
	)

	uuid = models.UUIDField(null=True, unique=True, default=uuid.uuid4, editable=False)
	email = models.EmailField(verbose_name='Email Address', max_length=255, unique=True)
	account_type = models.CharField(verbose_name='Account Type', max_length=50, choices=ACCOUNT_TYPE_CHOICES, default=ACCOUNT_TYPE_CHOICES[1])
	company_name = models.CharField(verbose_name='Company Name', max_length=300, blank=True, null=True)
	street = models.CharField(max_length=100, blank=True, null=True)
	city = models.CharField(max_length=100, blank=True, null=True)
	state = models.CharField(max_length=100, blank=True, null=True)
	country = CountryField(default='US', blank=True, null=True)
	postal_code = models.IntegerField(blank=True, null=True)
	first_name = models.CharField(max_length=200)
	middle_name = models.CharField(max_length=200, blank=True, null=True)
	last_name = models.CharField(max_length=200)
	phone_number = models.CharField(max_length=20, blank=True, null=True)
	is_active = models.BooleanField(default=True)
	is_admin = models.BooleanField(default=False)
	
	objects = UserManager()

	USERNAME_FIELD = 'email'
	REQUIRED_FIELDS = ['company_name', 'uuid', 'account_type', 'street', 'city', 'state', 'country', 'postal_code', 'first_name', 'middle_name', 'last_name', 'phone_number']

	def get_full_name(self):
		full_name = str(self.first_name) + ' ' + str(middle_name) + ' ' + str(last_name)
		return full_name.strip()

	def get_short_name(self):
		return self.last_name

	def __str__(self):
		return self.email

	def has_perm(self, perm, obj=None):
		return True

	def has_module_perms(self, app_label):
		return True

	@property
	def is_staff(self):
		return self.is_admin
