# cloudview

CloudView is a customer based machine vision project. The features that are provided by the system to the customer includes:
1. Face Detection and Recognition
2. People Counter
3. Face Clustering
4. Object Detection 
5. Object Tracking